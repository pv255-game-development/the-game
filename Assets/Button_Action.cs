﻿using UnityEngine;
using System.Collections;

public class Button_Action : MonoBehaviour {

	public Transform Point;
	public Transform Sphere;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseDown() {
		Vector3 dir = Point.position - Sphere.position;
		Sphere.rigidbody.AddForce (dir * 50.0f);
	}
}
