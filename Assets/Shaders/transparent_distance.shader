﻿Shader "Custom/transparent_distance" {
	Properties {
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}
	SubShader {
		Tags {"RenderType"="Transparent" "Queue"="Transparent+1"}
		Pass {
			Blend SrcAlpha OneMinusSrcAlpha
		
			CGPROGRAM
			
			#include "UnityCG.cginc"
			
			#pragma vertex vert
			#pragma fragment frag
			
			uniform float4 _Color;
			
			struct vertexInput {
				float4 vertex : POSITION;
			};
			
			struct vertexOutput {
				float4 pos : TEXCOORD0;
				float4 t_pos: SV_POSITION;
			};
			
			float nrand(float2 n )
			{
				return frac(sin(dot(n.xy, float2(12.9898, 78.233)))* 43758.5453);
			}
			
			float n3rand(float2 n )
			{
				float t = frac( _Time );
				float nrnd0 = nrand( n + 0.07*t );
				float nrnd1 = nrand( n + 0.11*t );
				float nrnd2 = nrand( n + 0.13*t );
				return (nrnd0+nrnd1+nrnd2) / 3.0;
			}
			
			vertexOutput vert(vertexInput v) {
				vertexOutput o;
				o.pos = mul(_Object2World, v.vertex);
				o.t_pos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}
			
			float4 frag(vertexOutput i) : COLOR {
				float3 vec = _WorldSpaceCameraPos - i.pos.xyz;
				float dist = length(vec);
				//float4 result = _Color;
				float4 result = n3rand(i.pos.xy).xxxx;
				//result.xyz = frac(i.pos.xyz);
				result.w = 1/(1+dist*dist);
				//result.w = 1.0f;
				return result;
			}
			
			ENDCG
		}
	}
	
	fallback "Diffuse"
}
