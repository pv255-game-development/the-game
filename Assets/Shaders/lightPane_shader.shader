﻿    Shader "Custom/lightPane_shader" {
    Properties {
        _MainTex ("Base (RGB), Emission (A)", 2D) = "white" {}
        _Bump ("Normal bump (RGB)", 2D) = "bump" {}
        _LightColor ("Light Color", Color) = (1, 1, 1, 1)
        _LightIntensity ("Light intensity multiplier", Range(1.0, 3.0)) = 1.0
        _OffColor ("Off Color", Color) = (0.07, 0.07, 0.07, 1)
        _PulseFreq ("Pulse frequency", Range(0.0, 10.0)) = 1.0
        _PulseOffset ("Pulse time offset", Float) = 0.0

    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200
  
        CGPROGRAM
        #pragma surface surf BlinnPhong
  
        sampler2D _MainTex;
        sampler2D _Bump;
        float _PulseFreq;
        float _PulseOffset;
        float _LightIntensity;
        float4 _LightColor;
        float4 _OffColor;

        struct Input {
            float2 uv_MainTex;
            float2 uv_Bump;
        };
  
        void surf (Input IN, inout SurfaceOutput o) {
            half4 c = tex2D (_MainTex, IN.uv_MainTex);

            o.Normal = UnpackNormal(tex2D(_Bump, IN.uv_Bump));  
            float delta = (sin(_Time.y * _PulseFreq + _PulseOffset) + 1) / 2;
            o.Emission = c.a * (delta * _LightColor  + (1 - delta) * _OffColor) * _LightIntensity;
            o.Albedo = c.rgb - (c.a * 0.9);
        }
        ENDCG
    }
    FallBack "Specular"
    }