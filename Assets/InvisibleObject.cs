﻿using UnityEngine;
using System.Collections;

public class InvisibleObject : MonoBehaviour {

	private GameWorldHandler mGameWorldHandler;
	private bool mLateInit;

	// Use this for initialization
	void Start () {
		mGameWorldHandler = transform.parent.gameObject.GetComponent<GameWorldHandler>();
		mLateInit = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(!mLateInit) {
			CastRays();
			mLateInit = true;
		}
	}

	protected void CastRays() {
		foreach (Transform child in transform)
		{
			var tile = mGameWorldHandler.FindTile(child.transform.position, -child.transform.forward);
			if(tile != null) {
				TileInteraction tileInteraction = tile.GetComponent<TileInteraction>();
				if(tileInteraction != null) {
					tileInteraction.IncCounter();
				}
			}
		}
	}
}
