﻿using UnityEngine;
using System.Collections;

public class Loading_Screen : MonoBehaviour {

	//public GameObject loadingBar;
	private string levelToLoad;

	// Use this for initialization
	void Start () {
		GameObject manager = GameObject.Find("_gameManager");
		GameManager gameManager = manager.GetComponent <GameManager>();
		levelToLoad = gameManager.levelToload;


		Application.LoadLevel (levelToLoad);
	}
	
	// Update is called once per frame
	void Update () {

	}
}
