﻿using UnityEngine;
using System.Collections;

public class Options : MonoBehaviour {

	public Font F;

	private float musicVolume;
	private float soundVolume;

	bool [] bResolutios;
	int [,] resolutions;

	bool fullscreen;

	bool musicMute;
	bool soundMute;

	int guiWidth = 350;
	int guiHeight = 325;
	int guiPositionX;
	int guiPositionY;

	SoundsManager soundsManager;
	
	// Use this for initialization
	void Start () {
			
		guiPositionX = (Screen.width - guiWidth) / 2;
		guiPositionY = (Screen.height - guiHeight) / 2;

		bResolutios = new bool[9];
		resolutions = new int[9,2] {{800,600},{1024,768},{1280,960},{1280,720},{1600,900},{1920,1080},{1280,800},{1440,900},{1680,1050}};

		findResolution ();

		fullscreen = Screen.fullScreen;

		GameObject manager = GameObject.Find("_gameManager");
		soundsManager = manager.GetComponent <SoundsManager>();

		musicMute = soundsManager.music.mute;
		soundMute = soundsManager.sound.mute;
		
		musicVolume = soundsManager.music.volume;
		soundVolume = soundsManager.sound.volume;

		//nastavit hlasitost hudby prodle hlasitosti prehravani ve scriptu prehravajicim hudbu

	}
	
	// Update is called once per frame
	void Update () {
		soundsManager.music.mute = musicMute;
		soundsManager.sound.mute = soundMute;
		soundsManager.music.volume = musicVolume;
		soundsManager.sound.volume = soundVolume;

					
	}


	void OnMouseDown()
	{

		

	}

	void findResolution()
	{
		for (int i=0; i<9; i++) 
		{
			if((Screen.width == resolutions[i,0])&&(Screen.height == resolutions[i,1]))
				bResolutios[i]=true;

		}
	}

	void getUsersResolution(int ResolutionsIndex)
	{
		for (int i=0; i<bResolutios.Length; i++) 
		{
			bResolutios[i]= false;
		}
		bResolutios [ResolutionsIndex] = true;
	}

	void OnGUI () 
	{
		GUI.skin.font = F;

				
		//guiText.fontSize = 30;

		GUI.Label (new Rect (guiPositionX, guiPositionY, 100, 30), "Video");
		
		GUI.Label (new Rect (guiPositionX, guiPositionY+50, 100, 30), "4:3");
		if (GUI.Toggle (new Rect (guiPositionX+50, guiPositionY+50, 100, 30), bResolutios [0], "800x600"))
			getUsersResolution (0);
		if(GUI.Toggle (new Rect (guiPositionX+150, guiPositionY+50, 100, 30), bResolutios[1], "1024x768"))
			getUsersResolution (1);
		if(GUI.Toggle (new Rect (guiPositionX+250, guiPositionY+50, 100, 30), bResolutios[2], "1280x960"))
			getUsersResolution (2);
		
		GUI.Label (new Rect (guiPositionX, guiPositionY+75, 100, 30), "16:10");
		if(GUI.Toggle (new Rect (guiPositionX+50,guiPositionY+75, 100, 30), bResolutios[3], "1280x720"))
			getUsersResolution (3);
		if(GUI.Toggle (new Rect (guiPositionX+150, guiPositionY+75, 100, 30), bResolutios[4], "1600x900"))
			getUsersResolution (4);
		if(GUI.Toggle (new Rect (guiPositionX+250, guiPositionY+75, 100, 30), bResolutios[5], "1920x1080"))
			getUsersResolution (5);
		
		GUI.Label (new Rect (guiPositionX, guiPositionY+100, 100, 30), "16:9");
		if(GUI.Toggle (new Rect (guiPositionX+50, guiPositionY+100, 100, 30), bResolutios[6], "1280x800"))
			getUsersResolution (6);
		if(GUI.Toggle (new Rect (guiPositionX+150, guiPositionY+100, 100, 30), bResolutios[7], "1440x900"))
			getUsersResolution (7);
		if(GUI.Toggle (new Rect (guiPositionX+250, guiPositionY+100, 100, 30), bResolutios[8], "1680x1050"))
			getUsersResolution (8);
		
		fullscreen = GUI.Toggle (new Rect (guiPositionX, guiPositionY+150, 100, 30), fullscreen, "Fullscreen");
		
		GUI.Label (new Rect (guiPositionX, guiPositionY+200, 100, 30), "Audio");
		
		GUI.Label (new Rect (guiPositionX, guiPositionY+250, 100, 30), "Music");
		musicVolume = GUI.HorizontalSlider (new Rect (guiPositionX+75, guiPositionY+257, 100, 30), musicVolume, 0.0f, 1.0f);
		musicMute = GUI.Toggle (new Rect (guiPositionX+200, guiPositionY+252, 100, 30), musicMute, "Mute music");
		
		
		GUI.Label (new Rect (guiPositionX, guiPositionY+275, 100, 30), "Sounds");
		soundVolume = GUI.HorizontalSlider (new Rect (guiPositionX+75, guiPositionY+282, 100, 30), soundVolume, 0.0f, 1.0f);
		soundMute = GUI.Toggle (new Rect (guiPositionX+200, guiPositionY+277, 100, 30), soundMute, "Mute Sounds");
		
		if (GUI.Button (new Rect (guiPositionX, guiPositionY+325, 100, 40), "OK"))
			saveChanges ();
		if (GUI.Button (new Rect (guiPositionX+200, guiPositionY+325, 100, 40), "Cancel"))
			closeOptions ();
	}

	void saveChanges()
	{
		for (int i=0; i<bResolutios.Length; i++)
		{
			if (bResolutios [i])
			{
				Debug.Log (resolutions [i, 0].ToString () + "x" + resolutions [i, 1].ToString ());
				Screen.SetResolution(resolutions[i,0],resolutions[i,1],fullscreen);
			}
		}

		soundsManager.saveSetup ();

		Application.LoadLevel ("Menu"); 
	}

	void closeOptions ()
	{
		Application.LoadLevel ("Menu"); 
	}
}
