﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	//______________________________________________________________
	private static GameManager _instance;
	
	public static GameManager instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<GameManager>();
				
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad(_instance.gameObject);
			}
			
			return _instance;
		}
	}
	
	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	//_______________________________________________________________


	public string levelToload;


	//private AudioClip c ;

	// Use this for initialization
	void Start () {
		//DontDestroyOnLoad (this);

	}
	
	// Update is called once per frame
	void Update () {

	}
}
