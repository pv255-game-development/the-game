﻿using UnityEngine;
using System.Collections;

public class Menu_BigWall_LightUp : MonoBehaviour {

	public float UpdateTime = 2.0f;
	private float mCounter = 0.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		mCounter += Time.deltaTime;
		if(mCounter > UpdateTime) {
			//change tiles
			mCounter = 0.0f;
		}

	}
}
