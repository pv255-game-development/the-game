﻿using UnityEngine;
using System.Collections;

public class Menu_Button_Behaviour : MonoBehaviour {

	public AudioClip mButtonHover;

	private SoundsManager soundsManager;

	// Use this for initialization
	void Start () {
		//mButtonHover = Resources.Load("Sounds/sound_menu_button_hover") as AudioClip;
		GameObject manager = GameObject.Find("_gameManager");
		soundsManager = manager.GetComponent <SoundsManager>(); 
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter() {
		guiText.material.color = Color.grey;
		//audio.PlayOneShot(mButtonHover);
		soundsManager.PlaySound (mButtonHover);
	}

	void OnMouseExit() {
		guiText.material.color = Color.white;
	}
}
