﻿using UnityEngine;
using System.Collections;

public class Menu_WallShifter : MonoBehaviour {

	public float Speed = 1.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//transform.position -= transform.right * Speed * Time.deltaTime;
		float max_x = float.NegativeInfinity;
		foreach (Transform child in transform) {
			child.position -= transform.right * Speed * Time.deltaTime;
			max_x = Mathf.Max(max_x, child.position.x);
		}
		foreach (Transform child in transform) {
			if(child.position.x < -35.0f) {
				Vector3 position = child.position;
				position.x = max_x + 16.0f;
				child.position = position;	
			}
		}
	}
}
