﻿using UnityEngine;
using System.Collections;

public class TileInteraction : MonoBehaviour {
	
	public Material MaterialTileOn;
	public Material MaterialTileReady;
	public Material MaterialTileOff;

	public enum State {
		Inactive,
		Ready,
		Active
	};

	private State mState;
	private int mCounter;
	private GameObject mGameWorld;
	private GameWorldHandler mGameWorldHandler;
	private Renderer mRenderer;

	// Use this for initialization
	void Start () {
		mCounter = 0;
		var wall = transform.parent.gameObject;
		mGameWorld = wall.transform.parent.gameObject;
		mGameWorldHandler = mGameWorld.GetComponent<GameWorldHandler>();
		mRenderer = GetComponentInChildren<Renderer>();
		SetTexture(State.Inactive);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Hit() {
		bool isLeftDown = Input.GetMouseButton(0);
		bool isRightDown = Input.GetMouseButton(1);

		if (isLeftDown && !isRightDown && (mState != State.Ready)) {
			print ("Set ready");
			Ready(true);
		} else if(isLeftDown && isRightDown && (mState == State.Ready)) {
			print ("Set unready");
			Unready(true);
		}
	}

	public void Ready(bool addToReadyTiles) {
		if(mState != State.Ready) {
			mRenderer.material = MaterialTileReady;
			if(addToReadyTiles) {
				mGameWorldHandler.AddReadyTile(gameObject);
			}
			mState = State.Ready;
		}
	}

	public void Unready(bool removeFromReadyTiles) {
		if(mCounter > 0) {
			SetTexture(State.Active);
			mState = State.Active;
		} else {
			SetTexture(State.Inactive);
			mState = State.Inactive;
		}
		if(removeFromReadyTiles) {
			mGameWorldHandler.RemoveReadyTile(gameObject);
		}
	}

	public void IncCounter() {
		mCounter++;
		if(mCounter > 0 && mState == State.Inactive) {
			SetTexture(State.Active);
			mState = State.Active;
		}
	}

	public void DecCounter() {
		mCounter--;
		if(mCounter <= 0 && mState == State.Active) {
			SetTexture(State.Inactive);
			mState = State.Inactive;
			mCounter = 0;
		}
	}

	public void SetTexture(State state) {
		switch(state) {
			case State.Inactive:
				mRenderer.material = MaterialTileOff;	
				break;
			case State.Ready:
				mRenderer.material = MaterialTileReady;	
				break;
			case State.Active:
				mRenderer.material = MaterialTileOn;	
				break;
		}
	}
}
