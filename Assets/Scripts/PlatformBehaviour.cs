﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlatformBehaviour : MonoBehaviour {

	public GameObject StartPosition;
	public GameObject EndPosition;
	public float Speed;

	private GameWorldHandler mGameWorldHandler;
	private List<TileInteraction> mLastUsedTiles;
	private float t;
	private float dir;

	// Use this for initialization
	void Start () {
		mGameWorldHandler = transform.parent.gameObject.GetComponent<GameWorldHandler>();
		mLastUsedTiles = new List<TileInteraction>();
		t = 0.0f;
		dir = 1.0f;
		Speed = 0.25f;
	}
	
	// Update is called once per frame
	void Update () {
		DeactiveUsedTiles();
		foreach (Transform child in transform)
		{
			var tile = mGameWorldHandler.FindTile(child.transform.position, -child.transform.forward);
			if(tile != null) {
				TileInteraction tileInteraction = tile.GetComponent<TileInteraction>();
				if(tileInteraction != null) {
					tileInteraction.IncCounter();
					mLastUsedTiles.Add(tileInteraction);
				}
			}
		}
		t += dir * Time.deltaTime * Speed;
		if( t > 1.0f) {
			dir = -1.0f;
		} else if (t < 0.0f) {
			dir = 1.0f;
		}
		transform.position = Vector3.Lerp(StartPosition.transform.position, EndPosition.transform.position, t);

	}

	private void DeactiveUsedTiles() {
		foreach(TileInteraction tileInteraction in mLastUsedTiles) {
			tileInteraction.DecCounter();
		}
		mLastUsedTiles.Clear();
	}
}
