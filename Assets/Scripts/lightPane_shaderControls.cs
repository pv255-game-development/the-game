﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class lightPane_shaderControls : MonoBehaviour {

    private enum ChangeParameter
    {
        LightColor,
        OffColor,
        PulseFreq
    };

    private class ChangeData
    {
        public ChangeParameter targetParameter;
        public object initialState;
        public object targetState;
        public float initialTime;
        public float targetTime;

        public override bool Equals(object obj)
        {
            return ReferenceEquals(this, obj);
        }
    }

    private static Color colorOff = new Color(0.05f, 0.05f, 0.05f);
    private static Color colorReady = new Color(0.6f, 0.8f, 0.6f);
    private static Color colorOn = new Color(1.0f, 1.0f, 1.0f);

    private LinkedList<ChangeData> m_changeData;
    private Renderer m_renderer;

    private float d_debugTime;

	// Use this for initialization
	void Start () 
    {
        m_renderer = GetComponentInChildren<Renderer>();
        m_changeData = new LinkedList<ChangeData>();
        setInactive();

        d_debugTime = Time.time + 4.0f;
	}
	
	// Update is called once per frame
	void Update () {

        UpdateChange();
        if (Time.time > d_debugTime)
        {
            setReady();
            d_debugTime += 10000;
        }
	}

    public void setInactive()
    {
        ChangeLightColorInTime(colorOff, 2.0f);
        ChangeOffColorInTime(colorOff, 2.0f);
        ChangePulseFreqIntime(0.0f, 2.0f);
    }

    public void setReady()
    {
        ChangeLightColorInTime(colorReady, 2.0f);
        ChangeOffColorInTime(colorOff, 2.0f);
        ChangePulseFreqIntime(2.0f, 2.0f);
    }

    public void setOn()
    {
        ChangeLightColorInTime(colorOn, 0.5f);
        ChangeOffColorInTime(colorOn, 0.5f);
        ChangePulseFreqIntime(0.0f, 0.1f);
    }

    //-------------------------------------------------------------------------

    private void ChangeLightColorInTime(Color targetColor, float time)
    {
        ChangeData changeData = new ChangeData();
        changeData.initialState = m_renderer.material.GetColor("_LightColor");
        changeData.targetState = targetColor;
        changeData.initialTime = Time.time;
        changeData.targetTime = changeData.initialTime + time;
        changeData.targetParameter = ChangeParameter.LightColor;
        m_changeData.AddLast(changeData);
    }

    private void ChangeOffColorInTime(Color targetColor, float time)
    {
        ChangeData changeData = new ChangeData();
        changeData.initialState = m_renderer.material.GetColor("_OffColor");
        changeData.targetState = targetColor;
        changeData.initialTime = Time.time;
        changeData.targetTime = changeData.initialTime + time;
        changeData.targetParameter = ChangeParameter.OffColor;
        m_changeData.AddLast(changeData);
    }

    private void ChangePulseFreqIntime(float targetFreq, float time)
    {
        ChangeData changeData = new ChangeData();
        changeData.initialState = m_renderer.material.GetFloat("_PulseFreq");
        changeData.targetState = targetFreq;
        changeData.initialTime = Time.time;
        changeData.targetTime = changeData.initialTime + time;
        changeData.targetParameter = ChangeParameter.PulseFreq;
        m_changeData.AddLast(changeData);
    }

    private void UpdateChange()
    {
        var chdNode = m_changeData.First;
        while(chdNode != null)
        {
            ChangeData chd = chdNode.Value;
            float delta = (Time.time - chd.initialTime) / (chd.targetTime - chd.initialTime);

            switch (chd.targetParameter)
            {
                case ChangeParameter.LightColor:
                {
                    float clampedDelta = Math.Min(1.0f, delta);
                    Color interpolated = clampedDelta * ((Color)chd.targetState) + (1 - clampedDelta) * ((Color)chd.initialState);
                    m_renderer.material.SetColor("_LightColor", interpolated);
                    break;
                }
                case ChangeParameter.OffColor:
                {
                    float clampedDelta = Math.Min(1.0f, delta);
                    Color interpolated = clampedDelta * ((Color)chd.targetState) + (1 - clampedDelta) * ((Color)chd.initialState);
                    m_renderer.material.SetColor("_OffColor", interpolated);
                    break;
                }
                case ChangeParameter.PulseFreq:
                {
                    float clampedDelta = Math.Min(1.0f, delta);
                    float interpolated = clampedDelta * ((float)chd.targetState) + (1 - clampedDelta) * ((float)chd.initialState);
                    m_renderer.material.SetFloat("_PulseFreq", interpolated);
                    break;
                }
            }

            var next = chdNode.Next;
            
            if (delta >= 1.0)
            {
                m_changeData.Remove(chdNode);
            }      

            chdNode = next;
        }
    }
}
