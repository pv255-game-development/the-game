﻿using UnityEngine;
using System.Collections;

public class WallBehaviour : MonoBehaviour {

	public Transform Tile;
	public int ColumnCount;
	public int RowCount;

	private Transform[] mTiles;
	private TileInteraction[] mTileInteractions;

	// Use this for initialization
	void Start () {
		//init arrays
		mTiles = new Transform[ColumnCount * RowCount];
		mTileInteractions = new TileInteraction[ColumnCount * RowCount];
		//create tiles
		for(int y=0; y<RowCount; y++) {
			for(int x=0; x<ColumnCount; x++) {
				Vector3 pos = transform.position + transform.right * (-ColumnCount/2+0.5f+x) + transform.up * (-RowCount/2+0.5f+y);
				mTiles[y*ColumnCount+x] = Instantiate(Tile, pos, transform.rotation * Quaternion.AngleAxis(180.0f, Vector3.left)) as Transform;
				mTiles[y*ColumnCount+x].parent = transform;
				mTileInteractions[y*ColumnCount+x] = mTiles[y*ColumnCount+x].gameObject.GetComponent<TileInteraction>();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	}

	public GameObject GetTile(RaycastHit hit) {
		int x = 0;
		int y = 0;
		//Debug.Log("hit coords: "+hit.barycentricCoordinate+" tri: "+hit.triangleIndex);
		if(hit.triangleIndex == 0) {
			x = (int)Mathf.Floor((1.0f-hit.barycentricCoordinate.x) * transform.localScale.x);
			y = (int)Mathf.Floor(hit.barycentricCoordinate.y * transform.localScale.y);
			//Debug.Log("tile: "+x+","+y);
		} else {
			x = (int)Mathf.Floor(hit.barycentricCoordinate.x * transform.localScale.x);
			y = (int)Mathf.Floor((1.0f-hit.barycentricCoordinate.y) * transform.localScale.y);
			//Debug.Log("tile: "+x+","+y);
		}
		return mTiles[y*ColumnCount+x].gameObject;
	}

	public void Hit(RaycastHit hit) {
		TileInteraction tileInteraction = GetTile(hit).GetComponent<TileInteraction>();
		tileInteraction.Hit();
	}
}
