﻿using UnityEngine;
using System.Collections;
using System.IO;

public class SoundsManager : MonoBehaviour {

	//public float musicVolume;
	//public float soundVolume;
	
	public AudioSource music;
	public AudioSource sound;

	// Use this for initialization
	void Start () {
		loadSetup ();

		music.Play();
	}
	
	// Update is called once per frame
	void Update () {
	 	//music.volume = musicVolume;
		//sound.volume = soundVolume;
	}

	public void PlaySound(AudioClip clip)
	{
		sound.clip = clip;
		sound.Play ();
	}

	public void setVolume(float mVolume, float sVolume)
	{
		music.volume = mVolume;
		sound.volume = sVolume;
	}

	public void saveSetup ()
	{
		StreamWriter soubor = new StreamWriter ("Data/Sounds.data");
		if (soubor != null) 
		{
			soubor.WriteLine(music.mute.ToString());
			soubor.WriteLine(sound.mute.ToString());
			soubor.WriteLine(music.volume.ToString());
			soubor.WriteLine(sound.volume.ToString());
		}
		soubor.Close ();
	}

	public void loadSetup ()
	{
		StreamReader soubor = new StreamReader ("Data/Sounds.data");
		if (soubor != null) 
		{
			if (soubor.ReadLine()== "True")
				music.mute=true;
			else
				music.mute=false;

			if (soubor.ReadLine()== "True")
				sound.mute=true;
			else
				sound.mute=false;
				
			music.volume = float.Parse(soubor.ReadLine());
			sound.volume = float.Parse(soubor.ReadLine());
			
		}
		soubor.Close ();
		
	}
}
