﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameWorldHandler : MonoBehaviour {

	public Transform InvisibleCube;
	private List<GameObject> mReadyTargetTiles;
	private List<GameObject> mReadyOppositeTiles;
	private List<Transform> mInvCubes;

	// Use this for initialization
	void Start () {
		//Screen.showCursor = false;
		Screen.lockCursor = true;
		mReadyTargetTiles = new List<GameObject>();
		mReadyOppositeTiles = new List<GameObject>();
		mInvCubes = new List<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
		bool isLeftDown = Input.GetMouseButton(0);
		bool isRightDown = Input.GetMouseButton(1);

		if(isLeftDown || isRightDown) {
			RaycastHit hit;
			Ray cameraRay = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
			int layerMask = 1 << 9;
			if (Physics.Raycast(cameraRay, out hit, Mathf.Infinity, layerMask)) {
				//print ("Hit");
				WallBehaviour wallBehaviour = hit.transform.gameObject.GetComponent<WallBehaviour>();
				if(wallBehaviour != null) {
					wallBehaviour.Hit(hit);
				}
			}
			if(!isLeftDown && isRightDown) {
				//print ("Right button");
				ConstructIntersectionObject();
				mReadyTargetTiles.Clear();
				mReadyOppositeTiles.Clear();
			}
		}
	}

	public void AddReadyTile(GameObject tile) {
		GameObject opposite_tile = FindTile(tile.transform.position, tile.transform.forward);
		if(opposite_tile != null) {
			print ("Opposite found");
			TileInteraction tileInteraction = opposite_tile.GetComponent<TileInteraction>();
			if(tileInteraction != null) {
				mReadyTargetTiles.Add(tile);
				tileInteraction.Ready(false);
				mReadyOppositeTiles.Add(opposite_tile);
			} else {
				print ("Opposite is not tile");
			}
		} else {
			print ("Opposite not found");
		}
	}

	public void RemoveReadyTile(GameObject tile) {
		GameObject opposite_tile = FindTile(tile.transform.position, tile.transform.forward);
		if(opposite_tile != null) {
			TileInteraction tileInteraction = opposite_tile.GetComponent<TileInteraction>();
			if(tileInteraction != null) {
				mReadyTargetTiles.Remove(tile);
				mReadyOppositeTiles.Remove(opposite_tile);
			}
		}
	}

	public GameObject FindTile(Vector3 pos, Vector3 dir) {
		int layerMask = 1 << 9;
		RaycastHit hit;
		if (Physics.Raycast(pos, dir, out hit, Mathf.Infinity, layerMask)) {
			if (hit.collider != null) {
				GameObject wall = hit.transform.gameObject;
				WallBehaviour wallBehaviour = wall.GetComponent<WallBehaviour>();
				if(wallBehaviour != null) {
					return wallBehaviour.GetTile(hit);
				}
			}
		}
		return null;
	}

	private void ConstructIntersectionObject() {
		int boxCount = 0;
		for(int i=0; i<mReadyTargetTiles.Count; ++i) {
			for(int j=(i+1); j<mReadyTargetTiles.Count; ++j) {
				Vector3 pos1 = mReadyTargetTiles[i].transform.position;
				Vector3 pos2 = mReadyTargetTiles[j].transform.position;
				Vector3 nor1 = mReadyTargetTiles[i].transform.forward;
				Vector3 nor2 = mReadyTargetTiles[j].transform.forward;
				TileInteraction targetTile1Interaction = mReadyTargetTiles[i].GetComponent<TileInteraction>();
				TileInteraction oppositeTile1Interaction = mReadyOppositeTiles[i].GetComponent<TileInteraction>();
				TileInteraction targetTile2Interaction = mReadyTargetTiles[j].GetComponent<TileInteraction>();
				TileInteraction oppositeTile2Interaction = mReadyOppositeTiles[j].GetComponent<TileInteraction>();
				//print("Nor1:"+nor1);
				//print("Nor2:"+nor2);
				//print("Dot:"+Vector3.Dot(nor1, nor2));
				bool xAxis = IsClose (pos1.x, pos2.x);
				bool yAxis = IsClose (pos1.y, pos2.y);
				bool zAxis = IsClose (pos1.z, pos2.z);
				if((xAxis || yAxis  || zAxis) && IsClose(Vector3.Dot(nor1, nor2), 0.0f)){
					nor1 = nor1 * Vector3.Dot(new Vector3(1,1,1), nor1);
					nor2 = nor2 * Vector3.Dot(new Vector3(1,1,1), nor2);
					Vector3 res = new Vector3(1, 1, 1) - nor1 - nor2;
					Vector3 new_pos = Vector3.Dot(pos1, nor2)*nor2 + Vector3.Dot(pos2, nor1)*nor1 + Vector3.Dot(pos1, res)*res;
					Vector3 cross = Vector3.Cross(nor1, nor2);
					GameObject thirdAxisTile = FindTile(new_pos, cross);
					GameObject thirdAxisOppositeTile = FindTile (new_pos, -cross);
					if(thirdAxisTile != null && thirdAxisOppositeTile != null) {
						TileInteraction thirdAxisTileInteraction = thirdAxisTile.GetComponent<TileInteraction>();
						TileInteraction thirdAxisOppositeTileInteraction = thirdAxisOppositeTile.GetComponent<TileInteraction>();
						//create cube
						if(!CheckIfInvCubeExists(new_pos) && thirdAxisOppositeTileInteraction != null && thirdAxisTileInteraction != null) {
							targetTile1Interaction.IncCounter();
							oppositeTile1Interaction.IncCounter();
							targetTile2Interaction.IncCounter();
							oppositeTile2Interaction.IncCounter();
							thirdAxisTileInteraction.IncCounter();
							thirdAxisOppositeTileInteraction.IncCounter();

							mInvCubes.Add(Instantiate(InvisibleCube, new_pos, Quaternion.identity) as Transform);
							boxCount++;
						}
					}
				}
			}
		}
		foreach(GameObject gameObject in mReadyTargetTiles) {
			TileInteraction tileInteraction = gameObject.GetComponent<TileInteraction>();
			tileInteraction.Unready(false);
		}
		foreach(GameObject gameObject in mReadyOppositeTiles) {
			TileInteraction tileInteraction = gameObject.GetComponent<TileInteraction>();
			tileInteraction.Unready(false);
		}
		print ("Box count: "+boxCount);
	}

	private bool IsClose(float a, float b) {
		return Mathf.Abs(a-b) < 0.01;
	}

	private bool CheckIfInvCubeExists(Vector3 pos) {
		foreach(Transform transform in mInvCubes) {
			if(Vector3.Distance(pos, transform.position) < 0.5f) {
				return true;
			}
		}
		return false;
	}
}
